"use strict"
/*
1-подія - це сигнал від браузера, про те, що щось сталось. й ми ці сигнали використовуємо. тобто, ми на їх вішаємо свою реакцію у вигляді функції

2- click, dblclick, mousemove, mouseover, mouseout, mouseup, mousedown,mouseenter, mouseleave, contextmenu

3- contextmenu це подія яка викликається правою кнопкою миші. завдяки contextmenu ми можемо ознайомитись із зазначеним обєктом
 використовувати цю подію можна по різному. на приклад ми можемо заборонити його користувачу
*/

//1-
const buttonClick = document.querySelector("#btn-click");
const sectionContent = document.querySelector("#content");

buttonClick.addEventListener('click', () => {
    const p = document.createElement("p");
    p.innerText = "New Paragraph";
   buttonClick.before(p);
});

//2-
const newButton = document.createElement("button");
newButton.id =  "btn-input-create";
newButton.innerText = "create input";

sectionContent.append(newButton);

newButton.addEventListener("click", () => {
    const input = document.createElement("input");
    input.id = "input-text";
    input.name = 'some-input';
    input.type = "text";
    input.placeholder = "you can take some txt";

    newButton.after(input);
});